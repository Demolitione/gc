﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Load : MonoBehaviour
{
    public InputField levelName;

    public void LoadTrigger()
    {
        if (levelName.text == "")
        {
            levelName.text = "Enter level name";
        }
        else
        {
            Camera.main.GetComponent<LevelEditor>().LoadLevel(levelName.text);
        }

    }
}
