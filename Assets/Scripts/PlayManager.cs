﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayManager : MonoBehaviour
{
    public string curLevelName;
    public int curLevelNumb = 0;

    void Start()
    {
        curLevelNumb = 1;
        Camera.main.GetComponent<LevelEditor>().LoadLevel("tutor" + curLevelNumb);
        curLevelName = "tutor";
    }

    private void Update()
    {
        Debug.Log(curLevelName);
    }

}
