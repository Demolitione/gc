﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{

    void Start()
    {
        Camera.main.GetComponent<LevelEditor>().edit = true;
    }

    void Update()
    {
        int ind = Camera.main.GetComponent<Control>().curState;

        switch (ind)
        {
            case 1:
                gameObject.GetComponent<Image>().color = Color.yellow;
                break;
            case 2:
                gameObject.GetComponent<Image>().color = Color.blue;
                break;
            case 3:
                gameObject.GetComponent<Image>().color = Color.black;
                break;
            case 4:
                gameObject.GetComponent<Image>().color = Color.red;
                break;
            case 5:
                gameObject.GetComponent<Image>().color = Color.white;
                break;
        }
        
    }
}
