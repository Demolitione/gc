﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class Save : MonoBehaviour
{
    public InputField levelName;

    public void SaveTrigger()
    {
        if (levelName.text == "")
        {
            levelName.text = "Enter level name";
        }
        else
        {
            Camera.main.GetComponent<LevelEditor>().ReadLevel(levelName.text);
        }

    }
}
