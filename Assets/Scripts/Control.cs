﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Control : MonoBehaviour
{
    public Vector2[] coords = new Vector2[10];

    public Vector2 offset;
    public int growPoints;
    public int k = 0;
    public int curState;
    public int winCap;
    public InputField console;

    public Queue<Vector2> queue = new Queue<Vector2>();

    

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);    
    }

    // Start is called before the first frame update
    void Start()
    {
        k = 0;
        winCap = 3;
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            GameObject.Find("Grow points").GetComponent<Text>().text = ("Grow points " + growPoints);
        }
        catch
        {
            
        }

        if (Input.GetKeyDown("z")) curState = 1;
        if (Input.GetKeyDown("x")) curState = 2;
        if (Input.GetKeyDown("c")) curState = 3;
        if (Input.GetKeyDown("v")) curState = 4;
        if (Input.GetKeyDown("b")) curState = 5;
        if (Input.GetKeyDown("`"))
        {
            if (console.IsActive())
            {
                console.gameObject.SetActive(false);
            }
            else
            {
                console.gameObject.SetActive(true);
            }    

        }

        try
        {
            GameObject.Find("GrowPointsText").GetComponent<Text>().text = "Grow points " + growPoints + " ";
        }

        catch
        {
            Debug.Log("editor");
        }


       if (winCap == 0)
        {
            StartCoroutine(Win());
            winCap = -1;
            Debug.Log("Game win");
        }
        Debug.Log("WinCap" + winCap);

        try
        {
            GameObject.Find("EditorText").GetComponent<Text>().text = " " + curState;
            Debug.Log(". . .");
        }
        catch
        {
            Debug.Log("No editor");
        }

    }

    public void save(float x, float y)
    {
        coords[k] = new Vector2(x, y);
        offset = coords[0];
        k++;
             
    }

    public void Grow()
    {
        StartCoroutine(Grow2());

    }

    public void Starter(Vector2 v2)
    {
        queue.Enqueue(v2);
    }

    public void EditorButton()
    {
        SceneManager.LoadScene("LevelField", LoadSceneMode.Single);
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("PlayField", LoadSceneMode.Single);
    }

    IEnumerator Win()
    {
        yield return new WaitForSeconds(1f);
        foreach (GameObject element in Camera.main.GetComponent<LevelGen>().prefabs)
        {
            if (element.GetComponent<SpriteRenderer>().color != Color.green)
            {
                element.GetComponent<SpriteRenderer>().color = Color.green;
                yield return new WaitForSeconds(0.01f);
            }

            
        }
        yield return new WaitForSeconds(1f);
        Debug.Log("Level name " + GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName);

        if ((GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName == "tutor") & (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb == 3))
        {
            GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb = 1;
            GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName = "level";
            gameObject.GetComponent<LevelEditor>().LoadLevel("level" + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
        }
        else
        {
            if (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName.Contains("tutor"))
            {
                GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb++;
                gameObject.GetComponent<LevelEditor>().LoadLevel("tutor" + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
            }
            else
            {
                GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb++;
                gameObject.GetComponent<LevelEditor>().LoadLevel("level" + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
            }
            
            
        }
        for (int i = 0; i > k; i++)
        {
            coords[i] = new Vector2(0, 0);
        }

        k = 0;
        
    }

    IEnumerator Grow2()
    {
        

        for (int j = 0; j < queue.Count;)
        {
            for (int i = 0; i < k; i++)
            {
                yield return new WaitForSeconds(0.03f);
                Camera.main.GetComponent<Tree>().GrowUp(coords[i] + queue.Peek() - offset);
            }
            queue.Dequeue();
        }
        check();
    }

    public void check()
    {
        if ((growPoints <= 0) & (winCap > 0))
        {
            gameObject.GetComponent<Tree>().OutOfGp();
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

}
