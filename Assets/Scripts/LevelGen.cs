﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelGen : MonoBehaviour
{
    public GameObject tile;
    public float weight = 0;
    public float height = 0;
    public int growPoints = 0;
    public int winCap = 0;

    public Vector2[] yellow = new Vector2[99];
    public Vector2[] black = new Vector2[99];
    public Vector2[] blue = new Vector2[99];
    public Vector2[] red = new Vector2[99];

    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;
    public GameObject button6;

    public Slider slider1;
    public Slider slider2;
    public List<GameObject> prefabs = new List<GameObject>();

    void Start()
    {


        
    }

    private void Update()
    {
        /*
        if (Input.GetKeyDown("1")) Level1();
        if (Input.GetKeyDown("2")) Level2();
        if (Input.GetKeyDown("3")) Level3();
        if (Input.GetKeyDown("4")) Level4();
        if (Input.GetKeyDown("5")) Level5();
        if (Input.GetKeyDown("6")) Level6();
        */
    }
    /*
    public void next()
    {
        switch (i)
        {
            case 1:
                Level1();
                i++;
                break;
            case 2:
                Level2();
                i++;
                break;
            case 3:
                Level3();
                i++;
                break;
            case 4:
                Level4();
                i++;
                break;
            case 5:
                Level5();
                i++;
                break;
            case 6:
                Level6();
                i++;
                break;

        }

    }
    
    public void Level1()
    {
        //button1.SetActive(false);
        //button2.SetActive(false);
        //button3.SetActive(false);
        //button4.SetActive(false);
        //button5.SetActive(false);
        //button6.SetActive(false);
        Camera.main.GetComponent<Control>().growPoints = 3;
        Camera.main.GetComponent<Control>().winCap = 5;
        weight = 5;
        height = 5;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
                
                
            }
        }

        

        GameObject.Find("x1y2").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x2y3").GetComponent<SpriteRenderer>().color = (Color.yellow);

        GameObject.Find("x3y2").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x4y3").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x3y4").GetComponent<SpriteRenderer>().color = (Color.blue);

        GameObject.Find("x1y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x4y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x4y1").GetComponent<SpriteRenderer>().color = (Color.black);
    }

    public void Level2()
    {
        button1.SetActive(false);
        button2.SetActive(false);
        button3.SetActive(false);
        button4.SetActive(false);
        button5.SetActive(false);
        button6.SetActive(false);
        Camera.main.GetComponent<Control>().growPoints = 3;
        Camera.main.GetComponent<Control>().winCap = 3;
        weight = 5;
        height = 5;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
            }
        }
        GameObject.Find("x2y2").GetComponent<SpriteRenderer>().color = (Color.yellow);

        GameObject.Find("x3y4").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x4y4").GetComponent<SpriteRenderer>().color = (Color.blue);

        GameObject.Find("x1y2").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x3y1").GetComponent<SpriteRenderer>().color = (Color.black);
    }

    public void Level3()
    {
        button1.SetActive(false);
        button2.SetActive(false);
        button3.SetActive(false);
        button4.SetActive(false);
        button5.SetActive(false);
        button6.SetActive(false);
        Camera.main.GetComponent<Control>().growPoints = 3;
        Camera.main.GetComponent<Control>().winCap = 5;
        weight = 5;
        height = 5;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
            }
        }
        GameObject.Find("x1y1").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x2y2").GetComponent<SpriteRenderer>().color = (Color.yellow);

        GameObject.Find("x1y2").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x2y3").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x3y3").GetComponent<SpriteRenderer>().color = (Color.blue);

        GameObject.Find("x0y3").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x0y4").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y3").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y4").GetComponent<SpriteRenderer>().color = (Color.black);

        GameObject.Find("x3y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x4y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x3y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x4y1").GetComponent<SpriteRenderer>().color = (Color.black);
    }


    public void Level4()
    {
        button1.SetActive(false);
        button2.SetActive(false);
        button3.SetActive(false);
        button4.SetActive(false);
        button5.SetActive(false);
        button6.SetActive(false);
        Camera.main.GetComponent<Control>().growPoints = 5;
        Camera.main.GetComponent<Control>().winCap = 5;
        weight = 7;
        height = 7;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
            }
        }
        GameObject.Find("x1y2").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x3y4").GetComponent<SpriteRenderer>().color = (Color.yellow);

        GameObject.Find("x3y6").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x3y3").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x5y5").GetComponent<SpriteRenderer>().color = (Color.blue);

        GameObject.Find("x0y3").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x0y4").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y3").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y4").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x3y5").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x5y6").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x5y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x5y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y2").GetComponent<SpriteRenderer>().color = (Color.black);

        GameObject.Find("x0y2").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x1y5").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x4y6").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x5y3").GetComponent<SpriteRenderer>().color = (Color.red);

    }

    public void Level5()
    {
        button1.SetActive(false);
        button2.SetActive(false);
        button3.SetActive(false);
        button4.SetActive(false);
        button5.SetActive(false);
        button6.SetActive(false);
        Camera.main.GetComponent<Control>().growPoints = 5;
        Camera.main.GetComponent<Control>().winCap = 6;
        weight = 7;
        height = 7;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
            }
        }
        GameObject.Find("x3y1").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x3y4").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x4y3").GetComponent<SpriteRenderer>().color = (Color.yellow);

        GameObject.Find("x4y6").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x5y5").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x6y2").GetComponent<SpriteRenderer>().color = (Color.blue);

        GameObject.Find("x0y2").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x2y3").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x4y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x4y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y5").GetComponent<SpriteRenderer>().color = (Color.black);

        GameObject.Find("x1y5").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x2y2").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x3y0").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x4y2").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x4y5").GetComponent<SpriteRenderer>().color = (Color.red);

    }

    public void Level6()
    {
        button1.SetActive(false);
        button2.SetActive(false);
        button3.SetActive(false);
        button4.SetActive(false);
        button5.SetActive(false);
        button6.SetActive(false);
        Camera.main.GetComponent<Control>().growPoints = 5;
        Camera.main.GetComponent<Control>().winCap = 7;
        weight = 9;
        height = 9;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
            }
        }
        GameObject.Find("x2y6").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x4y3").GetComponent<SpriteRenderer>().color = (Color.yellow);
        GameObject.Find("x6y4").GetComponent<SpriteRenderer>().color = (Color.yellow);

        GameObject.Find("x3y5").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x5y2").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x7y3").GetComponent<SpriteRenderer>().color = (Color.blue);
        GameObject.Find("x8y5").GetComponent<SpriteRenderer>().color = (Color.blue);

        GameObject.Find("x0y2").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y2").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x2y2").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x3y2").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x1y6").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x2y7").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x3y6").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y5").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x6y6").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x7y1").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x8y0").GetComponent<SpriteRenderer>().color = (Color.black);
        GameObject.Find("x8y1").GetComponent<SpriteRenderer>().color = (Color.black);

        GameObject.Find("x2y4").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x2y5").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x3y7").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x5y4").GetComponent<SpriteRenderer>().color = (Color.red);
        GameObject.Find("x7y4").GetComponent<SpriteRenderer>().color = (Color.red);

    }*/

    public void CustomLevel(float i, float j)
    {
        if (prefabs.Count > 0)
        {

            foreach (GameObject element in prefabs)
            {
                Destroy(element);
            }

            prefabs.Clear();
        }

        weight = i;
        height = j;
        Camera.main.orthographicSize = (Math.Max(weight, height));

        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                prefabs.Add(GameObject.Find("Square Pref(Clone)"));
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);


            }
        }
    }

    public void LoadLevel()
    {
        Camera.main.orthographicSize = Math.Max(weight, height);
        Camera.main.GetComponent<Control>().winCap = winCap;
        Debug.Log(" yel " + yellow[0]);
        if (prefabs.Count > 0)
        {

            foreach (GameObject element in prefabs)
            {
                Destroy(element);
            }

            prefabs.Clear();
        }

        Debug.Log("LevelGen weight" + weight);
        int i = 0;
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {

                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                prefabs.Add(GameObject.Find("Square Pref(Clone)"));

                if (Array.Exists(yellow, element => element == new Vector2(x, y)))
                {
                    if ((yellow[0] == new Vector2(0, 0)) & (yellow[1] != new Vector2(0, 0)))
                    {
                        GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.yellow;
                    } else
                    {
                        if ((x == 0) & (y == 0))
                        {

                        } else
                        {
                            GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.yellow;
                        }
                    }
                }

                if (Array.Exists(black, element => element == new Vector2(x, y)))
                {
                    if ((black[0] == new Vector2(0, 0)) & (black[1] != new Vector2(0, 0)))
                    {
                        GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.black;
                    }
                    else
                    {
                        if ((x == 0) & (y == 0))
                        {

                        }
                        else
                        {
                            GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.black;
                        }
                    }
                }

                if (Array.Exists(blue, element => element == new Vector2(x, y)))
                {
                    if ((blue[0] == new Vector2(0, 0)) & (blue[1] != new Vector2(0, 0)))
                    {
                        GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.blue;
                    }
                    else
                    {
                        if ((x == 0) & (y == 0))
                        {

                        }
                        else
                        {
                            GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.blue;
                        }
                    }
                }

                if (Array.Exists(red, element => element == new Vector2(x, y)))
                {

                    if ((x == 0) & (y == 0)) //(x == 0) & (y == 0)   red[0] == new Vector2(0, 0)
                    {
                        if ((red[0] == new Vector2(0, 0)) & (red[1] != new Vector2(0, 0)))
                        {
                            GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }
                    else
                    {
                        if ((x == 0) & (y == 0))
                        {

                        }
                        else
                        {
                            GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }

                }
                GameObject.Find("Square Pref(Clone)").name = ("x" + x + "y" + y);
                i++;
            }
        }


    }

    public void reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
