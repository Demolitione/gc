﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Click : MonoBehaviour
{

    public Grid grid;
    public Vector3Int cell;
    
    

    private void OnMouseEnter()
    {
        if (Camera.main.GetComponent<LevelEditor>().edit == false)
        {
            if ((GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f)))
            {
                GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1);
            }
        }
        else
        {

            

        }


    }
    private void OnMouseExit()
    {

        if ((GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f)))
        {
            

        } else
        {
            if (GetComponent<SpriteRenderer>().color == new Color(0.5f, 0.5f, 0.5f, 1))
            {
                GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            }
        }




    }

    

    private void OnMouseDown()
    {
        if (Camera.main.GetComponent<LevelEditor>().edit == false)
        {
            if ((GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f)))
            {


            }
            else
            {

                if (Camera.main.GetComponent<Control>().growPoints > 0)
                {

                    if (GetComponent<SpriteRenderer>().color == new Color(0.5f, 0.5f, 0.5f, 1))
                    {
                        Camera.main.GetComponent<Control>().growPoints--;
                        GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                        string[] data = gameObject.name.Trim('x').Split('y');
                        Camera.main.GetComponent<Control>().save((float.Parse(data[0])), (float.Parse(data[1])));
                        Camera.main.GetComponent<Control>().check();
                    }
                    else
                    {

                        if (GetComponent<SpriteRenderer>().color == (Color.yellow))
                        {
                            Camera.main.GetComponent<Control>().winCap--;
                            Camera.main.GetComponent<Control>().growPoints = 0;
                            string[] data = gameObject.name.Trim('x').Split('y');

                            GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);

                            Camera.main.GetComponent<Control>().save((float.Parse(data[0])), (float.Parse(data[1])));
                            Camera.main.GetComponent<Control>().Starter(new Vector2((float.Parse(data[0])), (float.Parse(data[1]))));

                            Camera.main.GetComponent<Control>().Grow();
                        }
                    }
                }
            }
        }



        //GameObject.Find("x" + 3 + "y" + 3).GetComponent<Tree>().GrowUp(2, 2);
    }
}
