﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorText : MonoBehaviour
{
    public Text tip1;
    public Text tip2;
    public Text tip3;

    void Update()
    {
        if ((gameObject.GetComponent<PlayManager>().curLevelName == "tutor") &  (gameObject.GetComponent<PlayManager>().curLevelNumb == 1))
        {
            tip1.gameObject.SetActive(true);
            tip2.gameObject.SetActive(false);
            tip3.gameObject.SetActive(false);
        }
        else
        {
            if ((gameObject.GetComponent<PlayManager>().curLevelName == "tutor") & (gameObject.GetComponent<PlayManager>().curLevelNumb == 2))
            {
                tip1.gameObject.SetActive(false);
                tip2.gameObject.SetActive(true);
                tip3.gameObject.SetActive(false);
            }
            else
            {

                if ((gameObject.GetComponent<PlayManager>().curLevelName == "tutor") & (gameObject.GetComponent<PlayManager>().curLevelNumb == 3))
                {
                    tip1.gameObject.SetActive(false);
                    tip2.gameObject.SetActive(false);
                    tip3.gameObject.SetActive(true);
                }
                else
                {
                    tip1.gameObject.SetActive(false);
                    tip2.gameObject.SetActive(false);
                    tip3.gameObject.SetActive(false);
                }
            }
        }
        
    }

}
