﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditor : MonoBehaviour
{
    public InputField textField;
    public GameObject tile;
    public bool edit;

    void Update()
    {
        try
        {
            string text = textField.text;
            if (Input.GetKeyDown(KeyCode.KeypadEnter)) Comamnd(text);
        }
        catch
        {
            Debug.Log("not editor");
        }
    }

    public void Comamnd(string text)
    {
        string[] com = text.Split(' ');

        try
        {
            Debug.Log(com[0]);
            Debug.Log(com[1]);
            Debug.Log(com[2]);
        }
        catch
        {
            Debug.Log("low arg plug ");
        }
        
        if (com[0] == "save") ReadLevel(com[1]);
        if (com[0] == "load") LoadLevel(com[1]);
        if (com[0] == "gen") Camera.main.GetComponent<LevelGen>().CustomLevel(float.Parse(com[1]), float.Parse(com[2]));
        if (com[0] == "edit")
        {
            if (edit == true)
            {
                edit = false;
            }
            else
            {
                edit = true;
            }
        }
            
    }

    public void ReadLevel(string arg1)
    {

        float weight = Camera.main.GetComponent<LevelGen>().weight;
        float height = Camera.main.GetComponent<LevelGen>().height;
        int bla = 0;
        int yel = 0;
        int blu = 0;
        int red = 0;
        int wC = 0;
        Debug.Log("w " + weight);
        Debug.Log("h " + height);

        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                
                Debug.Log(GameObject.Find("x" + x + "y" + y).name);
                tile = GameObject.Find("x" + (int)x + "y" + (int)y);

                gameObject.GetComponent<SaveLoad>().level = arg1;
                if (tile.GetComponent<SpriteRenderer>().color == Color.black)
                {
                    gameObject.GetComponent<SaveLoad>().curBla[bla, 0] = x;
                    gameObject.GetComponent<SaveLoad>().curBla[bla, 1] = y;
                    bla++;
                } else
                {
                    if (tile.GetComponent<SpriteRenderer>().color == Color.yellow)
                    {
                        gameObject.GetComponent<SaveLoad>().curYe[yel, 0] = x;
                        gameObject.GetComponent<SaveLoad>().curYe[yel, 1] = y;
                        yel++;
                        wC++;
                    }
                    else
                    {
                        if (tile.GetComponent<SpriteRenderer>().color == Color.blue)
                        {
                            gameObject.GetComponent<SaveLoad>().curBlu[blu, 0] = x;
                            gameObject.GetComponent<SaveLoad>().curBlu[blu, 1] = y;
                            blu++;
                            wC++;
                        }
                        else
                        {
                            if (tile.GetComponent<SpriteRenderer>().color == Color.red)
                            {
                                gameObject.GetComponent<SaveLoad>().curRe[red, 0] = x;
                                gameObject.GetComponent<SaveLoad>().curRe[red, 1] = y;
                                red++;
                            }
                        }
                    }
                }
            }
        }

        gameObject.GetComponent<SaveLoad>().curWC = wC;
        gameObject.GetComponent<SaveLoad>().curGP = wC;
        gameObject.GetComponent<SaveLoad>().curWe = (int)weight;
        gameObject.GetComponent<SaveLoad>().curHe = (int)height;

        gameObject.GetComponent<SaveLoad>().SaveLevel();
    }

    public void LoadLevel(string name)
    {
        gameObject.GetComponent<SaveLoad>().level = name;
        gameObject.GetComponent<SaveLoad>().LoadLevel();

    }

}
